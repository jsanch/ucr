#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This module contains helper functions used by the nodes on the ucr_pose_filtering package.

"""

import tf
import geometry_msgs.msg
import kuka_lwr_controllers.msg


def pose_array_to_list(poses):
    """
    Returns a geometry_msgs.msg.PoseArray message as
    a generator of geometry_msgs.msg.PoseStamped objects.

    :param poses: An array of poses.
    :type poses: geometry_msgs.msg.PoseArray

    :return: A generator containing a set of geometry_msgs.msg.PoseStamped objects.
    :rtype: generator

    """
    assert (type(poses) == geometry_msgs.msg.PoseArray),\
        "'poses' must be of type 'geometry_msgs.msg.PoseArray'."

    return (geometry_msgs.msg.PoseStamped(poses.header, pose) for pose in poses.poses)


def pose_to_kuka_message(pose_in):
    """
    Transforms a pose_in message into a kuka_lwr_controllers/PoseRPY message.

    :param pose_in: The pose_in to be converted.
    :type pose_in: geometry_msgs.msg.Pose

    :return: The converted message.
    :rtype: kuka_lwr_controllers.msg.PoseRPY

    """
    assert (type(pose_in) == geometry_msgs.msg.Pose),\
        "'pose_in' must be of type 'geometry_msgs.msg.Pose'."

    q = pose_in.orientation
    rpy = tf.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w])
    return kuka_lwr_controllers.msg.PoseRPY(
        id=0, position=pose_in.position, orientation=kuka_lwr_controllers.msg.RPY(*rpy))
