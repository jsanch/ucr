#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This module publishes the set of poses that are solvable by inverse kinematics (IK)
for the specified arm.

"""

import rospy
import actionlib
import std_msgs.msg
import geometry_msgs.msg
import kuka_lwr_controllers.msg
import moveit_msgs.msg
import ucr_manipulation_utils_ros.kinematics as kinematics
import ucr_pose_filtering.utils as utils


class PoseFilter(object):
    """
    Publishes a set of poses that are solvable by inverse kinematics.

    """
    def __init__(self):
        """
        Returns a pose filter object.

        :return: Pose filter.
        :rtype: ik_based_filter_node.PoseFilter

        """
        # Params
        self.event = None
        self.poses_in = None
        
        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10.0))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=1)
        self.poses_out = rospy.Publisher(
            "~poses_out", kuka_lwr_controllers.msg.PoseRPY, queue_size=1)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber(
            "~poses_in", geometry_msgs.msg.PoseArray, self.poses_in_cb)

        # Wait for MoveIt!
        move_group = rospy.get_param('~move_group', None)
        assert move_group is not None, "Move group must be specified."
        client = actionlib.SimpleActionClient(move_group, moveit_msgs.msg.MoveGroupAction)
        rospy.loginfo("Waiting for '{0}' server".format(move_group))
        client.wait_for_server()
        rospy.loginfo("Found server '{0}'".format(move_group))

        # Group on which inverse kinematics (IK) solutions will be queried.
        self.arm = rospy.get_param('~arm', None)
        assert self.arm is not None, "Group to move (e.g. arm) must be specified."

        # Kinematics class to compute the inverse kinematics.
        self.kinematics = kinematics.Kinematics(self.arm)

        # Time allowed for the IK solver to find a solution (in seconds).
        self.ik_timeout = rospy.get_param('~ik_timeout', 0.5)

    def event_in_cb(self, msg):
        """
        Obtains an event for the component.

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def poses_in_cb(self, msg):
        """
        Obtains an array of poses.

        :param msg: List of poses.
        :type msg: geometry_msgs.msg.PoseArray

        """
        self.poses_in = msg

    def start(self):
        """
        Starts the component.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.poses_in is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            pose = self.select_reachable_pose(self.poses_in)
            if pose:
                msg = utils.pose_to_kuka_message(pose.pose)
                self.poses_out.publish(msg)
            else:
                rospy.logwarn("No solution was found.")
            self.reset_component_data()
            return 'IDLE'

    def group_goal_poses(self):
        """
        Returns a list of PoseStamped objects based on the input to the node.

        :return: A list of the goal poses.
        :rtype: list or None

        """
        poses = None
        if self.poses_in:
            poses = utils.pose_array_to_list(self.poses_in)
        return poses

    def select_reachable_pose(self, poses):
        """
        Given a list of poses, it returns the first pose that is a solution.

        :param poses: A list of geometry_msgs.msg.PoseStamped objects.
        :type poses: list

        :return: The pose for which a solution exists.
        :rtype: geometry_msgs.msg.PoseStamped or None

        """
        for ii, pose in enumerate(utils.pose_array_to_list(poses)):
            rospy.logdebug("IK solver attempt number: {0}".format(ii))
            solution = self.kinematics.inverse_kinematics(
                pose, timeout=self.ik_timeout)
            if solution:
                return pose
        return None

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.poses_in = None
        self.event = None


def main():
    rospy.init_node("pose_filter", anonymous=True)
    pose_selector = PoseFilter()
    pose_selector.start()
