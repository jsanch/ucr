#!/usr/bin/env python
"""
Test unit for the functions in the utils.py module.

"""

import math
import numpy.testing as testing
import unittest
import rosunit
import std_msgs.msg
import geometry_msgs.msg
import kuka_lwr_controllers.msg
import ucr_pose_filtering.utils as utils

PKG = 'ucr_pose_filtering'


def assert_almost_equal_pose_rpy(actual, desired, decimal=5):
    """
    Helper function to test if two kuka_lwr_controllers/PoseRPY message
    are equal up to desired precision.

    :param actual:
    :type actual: kuka_lwr_controllers/PoseRPY

    :param desired:
    :type actual: kuka_lwr_controllers/PoseRPY

    :param decimal: Desired precision, default is 7.
    :type decimal: int, optional

    :raises: AssertionError if actual and desired are not equal up to specified precision.

    """
    testing.assert_equal(actual.id, desired.id)
    testing.assert_equal(actual.position.x, desired.position.x)
    testing.assert_equal(actual.position.y, desired.position.y)
    testing.assert_equal(actual.position.z, desired.position.z)
    testing.assert_almost_equal(
        actual.orientation.roll, desired.orientation.roll, decimal=decimal)
    testing.assert_almost_equal(
        actual.orientation.pitch, desired.orientation.pitch, decimal=decimal)
    testing.assert_almost_equal(
        actual.orientation.yaw, desired.orientation.yaw, decimal=decimal)


class TestIKBasedFilter(unittest.TestCase):
    """
    Tests functions used in the utils.py module.

    """
    def test_pose_array_to_list_bad_input(self):
        """
        Tests that the 'pose_array_to_list' function raises an exception caused
        by specifying a wrong argument.

        """
        pose_1 = 3          # scalar instead of geometry_msgs.msg.PoseArray
        pose_2 = [1, 2, 3]  # list instead of geometry_msgs.msg.PoseArray

        # geometry_msgs.msg.PoseStamped instead of geometry_msgs.msg.PoseArray
        pose_3 = geometry_msgs.msg.PoseStamped()
        pose_3.pose.position.x = 1.0
        pose_3.pose.position.y = 2.0
        pose_3.pose.position.z = 3.0

        with self.assertRaises(AssertionError):
            utils.pose_array_to_list(pose_1)
        with self.assertRaises(AssertionError):
            utils.pose_array_to_list(pose_2)
        with self.assertRaises(AssertionError):
            utils.pose_array_to_list(pose_3)

    def test_pose_array_to_list_basic(self):
        """
        Tests that the 'pose_array_to_list' function returns a generator of
        geometry_msgs.msg.PoseStamped objects.

        """
        pose_header = std_msgs.msg.Header(frame_id="base_link")

        pose_1 = geometry_msgs.msg.Pose()
        pose_1.position.x = 1.0
        pose_1.position.y = 2.0
        pose_1.position.z = 3.0

        pose_2 = geometry_msgs.msg.Pose()
        pose_2.position.x = 4.0
        pose_2.position.y = 5.0
        pose_2.position.z = 6.0

        pose_3 = geometry_msgs.msg.Pose()
        pose_3.position.x = 5.0
        pose_3.position.y = 8.0
        pose_3.position.z = 9.0

        poses = geometry_msgs.msg.PoseArray(
            header=pose_header, poses=[pose_1, pose_2, pose_3]
        )

        desired = [
            geometry_msgs.msg.PoseStamped(header=pose_header, pose=pose_1),
            geometry_msgs.msg.PoseStamped(header=pose_header, pose=pose_2),
            geometry_msgs.msg.PoseStamped(header=pose_header, pose=pose_3)
        ]

        actual = list(utils.pose_array_to_list(poses))
        self.assertEqual(actual, desired)

    def test_pose_to_kuka_message_bad_input(self):
        """
        Tests that the 'pose_to_kuka_message' function raises an exception caused
        by specifying a wrong argument.

        """
        pose_1 = 3          # scalar instead of geometry_msgs.msg.Pose
        pose_2 = [1, 2, 3]  # list instead of geometry_msgs.msg.Pose

        # geometry_msgs.msg.PoseStamped instead of geometry_msgs.msg.PoseArray
        pose_3 = geometry_msgs.msg.PoseStamped()
        pose_3.pose.position.x = 1.0
        pose_3.pose.position.y = 2.0
        pose_3.pose.position.z = 3.0

        with self.assertRaises(AssertionError):
            utils.pose_to_kuka_message(pose_1)
        with self.assertRaises(AssertionError):
            utils.pose_to_kuka_message(pose_2)
        with self.assertRaises(AssertionError):
            utils.pose_to_kuka_message(pose_3)

    def test_pose_to_kuka_message_basic(self):
        """
        Tests that the 'pose_to_kuka_message' function returns a
        kuka_lwr_controllers/PoseRPY message.

        """
        pose_in = geometry_msgs.msg.Pose()
        pose_in.position.x = 1.0
        pose_in.position.y = 2.0
        pose_in.position.z = 3.0
        pose_in.orientation.x = 0.0
        pose_in.orientation.y = 0.0
        pose_in.orientation.z = 0.0
        pose_in.orientation.w = 1.0

        desired = kuka_lwr_controllers.msg.PoseRPY()
        desired.id = 0
        desired.position.x = 1.0
        desired.position.y = 2.0
        desired.position.z = 3.0
        desired.orientation.roll = 0.0
        desired.orientation.pitch = 0.0
        desired.orientation.yaw = 0.0

        actual = utils.pose_to_kuka_message(pose_in)
        assert_almost_equal_pose_rpy(actual, desired)

    def test_pose_to_kuka_message_roll(self):
        """
        Tests that the 'pose_to_kuka_message' function returns a
        kuka_lwr_controllers/PoseRPY message for a non-zero roll angle.

        """
        pose_in = geometry_msgs.msg.Pose()
        pose_in.position.x = 1.0
        pose_in.position.y = 2.0
        pose_in.position.z = 3.0
        pose_in.orientation.x = 0.70711
        pose_in.orientation.y = 0.0
        pose_in.orientation.z = 0.0
        pose_in.orientation.w = 0.70711

        desired = kuka_lwr_controllers.msg.PoseRPY()
        desired.id = 0
        desired.position.x = 1.0
        desired.position.y = 2.0
        desired.position.z = 3.0
        desired.orientation.roll = math.radians(90)
        desired.orientation.pitch = 0.0
        desired.orientation.yaw = 0.0

        actual = utils.pose_to_kuka_message(pose_in)
        assert_almost_equal_pose_rpy(actual, desired)

    def test_pose_to_kuka_message_pitch(self):
        """
        Tests that the 'pose_to_kuka_message' function returns a
        kuka_lwr_controllers/PoseRPY message for a non-zero pitch angle.

        """
        pose_in = geometry_msgs.msg.Pose()
        pose_in.position.x = 1.0
        pose_in.position.y = 2.0
        pose_in.position.z = 3.0
        pose_in.orientation.x = 0.0
        pose_in.orientation.y = 0.70711
        pose_in.orientation.z = 0.0
        pose_in.orientation.w = 0.70711

        desired = kuka_lwr_controllers.msg.PoseRPY()
        desired.id = 0
        desired.position.x = 1.0
        desired.position.y = 2.0
        desired.position.z = 3.0
        desired.orientation.roll = 0.0
        desired.orientation.pitch = math.radians(90)
        desired.orientation.yaw = 0.0

        actual = utils.pose_to_kuka_message(pose_in)
        assert_almost_equal_pose_rpy(actual, desired)

    def test_pose_to_kuka_message_yaw(self):
        """
        Tests that the 'pose_to_kuka_message' function returns a
        kuka_lwr_controllers/PoseRPY message for a non-zero yaw angle.

        """
        pose_in = geometry_msgs.msg.Pose()
        pose_in.position.x = 1.0
        pose_in.position.y = 2.0
        pose_in.position.z = 3.0
        pose_in.orientation.x = 0.0
        pose_in.orientation.y = 0.0
        pose_in.orientation.z = 0.70711
        pose_in.orientation.w = 0.70711

        desired = kuka_lwr_controllers.msg.PoseRPY()
        desired.id = 0
        desired.position.x = 1.0
        desired.position.y = 2.0
        desired.position.z = 3.0
        desired.orientation.roll = 0.0
        desired.orientation.pitch = 0.0
        desired.orientation.yaw = math.radians(90)

        actual = utils.pose_to_kuka_message(pose_in)
        assert_almost_equal_pose_rpy(actual, desired)


if __name__ == '__main__':
    rosunit.unitrun(PKG, 'test_ik_based_filter', TestIKBasedFilter)
