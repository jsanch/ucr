#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Test integration for the pose_filter_test node.

"""

import unittest
import rostest
import rospy
import std_msgs.msg
import geometry_msgs.msg
import kuka_lwr_controllers.msg

PKG = 'ucr_pose_filtering'


class TestPoseFilter(unittest.TestCase):
    def setUp(self):
        """
        Sets up the test fixture before exercising it.

        """
        # Params
        self.result = None
        self.wait_for_result = None

        # Publishers
        self.event_out = rospy.Publisher('~event_out', std_msgs.msg.String, latch=True)
        self.poses = rospy.Publisher('~poses', geometry_msgs.msg.PoseArray)

        # Subscribers
        self.component_output = rospy.Subscriber(
            '~component_output', kuka_lwr_controllers.msg.PoseRPY, self.result_callback)

    def tearDown(self):
        """
        Deconstructs the test fixture after testing it.

        """
        self.event_out.unregister()
        self.poses.unregister()
        self.component_output.unregister()

    def test_pose_filter(self):
        """
        Verifies that the node publishes a kuka_lwr_controllers/PoseRPY message.
        Note: this is not a functionality test.

        """
        poses = geometry_msgs.msg.PoseArray()
        poses.header.frame_id = 'base_link'

        pose_1 = geometry_msgs.msg.Pose()
        pose_2 = geometry_msgs.msg.Pose()

        poses.poses.append(pose_1)
        poses.poses.append(pose_2)

        while not self.wait_for_result:
            self.poses.publish(poses)
            self.event_out.publish('e_start')

        self.assertIsInstance(self.result, kuka_lwr_controllers.msg.PoseRPY)

    def result_callback(self, msg):
        self.result = msg
        self.wait_for_result = True


if __name__ == '__main__':
    rospy.init_node('pose_filter_test')
    rostest.rosrun(PKG, 'pose_filter_test', TestPoseFilter)
