#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['ucr_pose_filtering'],
    package_dir={
        'ucr_pose_filtering': 'src/ucr_pose_filtering'
    }
)

setup(**d)
