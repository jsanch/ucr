#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['ucr_pose_generation', 'ucr_pose_generation_ros'],
    package_dir={'ucr_pose_generation': 'common/src/ucr_pose_generation',
                 'ucr_pose_generation_ros': 'ros/src/ucr_pose_generation_ros'}
)

setup(**d)
