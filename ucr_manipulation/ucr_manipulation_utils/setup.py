#!/usr/bin/env python

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
   packages=['ucr_manipulation_utils_ros'],
   package_dir={'ucr_manipulation_utils_ros': 'ros/src/ucr_manipulation_utils_ros'}
)

setup(**d)
