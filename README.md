# UCA Common Robotics (UCR). 
This repository contains ROS packages to perform various tasks, e.g. collect data in experiments. 
It serves as a utility repository for the [UDOM](https://gitlab.com/jsanch/udom) package.
It was developed as part of my PhD thesis in Université Clermont Auvergne (UCA), France.

For further information, please consult the README file of this [repository](https://gitlab.com/jsanch/udom).
