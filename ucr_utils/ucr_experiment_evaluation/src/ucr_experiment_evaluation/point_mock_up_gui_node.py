#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publish a PointStamped message interactively based on the
controls of a GUI.


**Output(s):**
  * `point`: Point to be visualized.
    - *type:* `geometry_msgs/PointStamped`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).
  * `reference_frame`: Reference frame of the wrench to be published.

"""

import threading
import Tkinter
import numpy as np
import rospy
import tf.transformations as transformations
import std_msgs.msg
import geometry_msgs.msg

# In meters.
POSITION_RESOLUTION = 0.002
MAX_POS_X = 2.0
MIN_POS_X = -2.0
MAX_POS_Y = 2.0
MIN_POS_Y = -2.0
MAX_POS_Z = 2.0
MIN_POS_Z = -2.0

point = geometry_msgs.msg.PointStamped()

global lock
lock = threading.Lock()


def create_window():
    """
    Creates a GUI window to publish a pose.

    """
    master = Tkinter.Tk()

    # Position.
    position_x = Tkinter.Scale(
        master, command=update_position_x, from_=MAX_POS_X, to=MIN_POS_X,
        resolution=POSITION_RESOLUTION, label="Position X"
    )
    position_y = Tkinter.Scale(
        master, command=update_position_y, from_=MAX_POS_Y, to=MIN_POS_Y,
        resolution=POSITION_RESOLUTION, label="Position Y"
    )
    position_z = Tkinter.Scale(
        master, command=update_position_z, from_=MAX_POS_Z, to=MIN_POS_Z,
        resolution=POSITION_RESOLUTION, label="Position Z"
    )
    position_x.grid(row=0, column=0)
    position_y.grid(row=0, column=1)
    position_z.grid(row=0, column=2)

    master.title("Point mock-up")
    master.mainloop()
    rospy.signal_shutdown("GUI closed")


def update_position_x(slider):
    """
    Sets slider as the position in the X axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    point.point.x = float(slider)
    lock.release()


def update_position_y(slider):
    """
    Sets slider as the position in the Y axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    point.point.y = float(slider)
    lock.release()


def update_position_z(slider):
    """
    Sets slider as the position in the Z axis.

    :param slider: Value from the slider in the GUI.
    :type slider: str

    """
    global lock
    lock.acquire()
    point.point.z = float(slider)
    lock.release()


def publish_point():
    """
    Publishes the point.

    """
    # Node cycle rate (in Hz).
    loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 10))

    # Reference frame of the wrench.
    reference_frame = rospy.get_param('~reference_frame', 'map')

    # Publishers
    pub_point = rospy.Publisher(
        '~point_out', geometry_msgs.msg.PointStamped, queue_size=10)

    point.header.frame_id = reference_frame
    while not rospy.is_shutdown():
        point.header.stamp = rospy.Time.now()

        pub_point.publish(point)
        loop_rate.sleep()


def main():
    rospy.init_node('point_mock_up')

    import thread
    try:
        thread.start_new_thread(create_window, tuple())

        publish_point()
    except rospy.ROSInterruptException:
        pass
