#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
This node publishes a discretized workspace of a robot as well
as the position that have been covered by the robot's end-effector.

**Input(s):**
  * `current_point`: Point representing the position of the robot's end-effector.
    - *type:* `geometry_msgs/PointStamped
  * `event_in`: The desired event for the node:
      `e_start`: starts the component.
      `e_stop`: stops the component.
    - *type:* `std_msgs/String`

**Output(s):**
  * `covered_points`: Points that have been covered by the end-effector.
    - *type:* `visualization_msgs/Marker`
  * `workspace`: Points describing the robot's workspace
    - *type:* `visualization_msgs/Marker`
  * `event_out`: The current event of the node.
      `e_running`: when the component is running.
      `e_stopped`: when the component is stopped.
    - *type:* `std_msgs/String`

**Parameter(s):**
  * `loop_rate`: Node cycle rate (in Hz).
  * `tolerance`: Minimum distance required between to points to consider them in the same position (in meters).
  * `reference_frame`: Reference frame of the mesh.

"""

import numpy as np
import rospy
import rospkg
import std_msgs.msg
import geometry_msgs.msg
import visualization_msgs.msg
import udom_geometric_transformation.transformation_utils as transformation_utils


class TrackWorkspaceCoverageNode(object):
    """
    Subscribes to a PointStamped message describing the robot's end-effector
    position and publishes a discretized workspace of a robot as well
    as the positions that have been covered by the robot's end-effector.

    """
    def __init__(self):
        """
        Instantiates a TrackWorkspaceCoverage node.

        :return: Node to publish the workspace coverage.
        :rtype: TrackWorkspaceCoverageNode

        """
        rospack = rospkg.RosPack()

        # Params
        self.event = None

        package_path = rospack.get_path('ucr_experiment_evaluation')
        self.config_path = package_path + '/config/'
        # Numpy file containing an array describing a discretized workspace.
        # The array should be of shape 3xN, where N represents the number of points.
        self.workspace = np.load(self.config_path + rospy.get_param('~workspace') + '.npy').T
        self.covered_points_idx = set()
        self.current_point = None

        # Object to compute transformations.
        self.listener = transformation_utils.GeometryTransformer()

        # Minimum distance required between to points to consider them in the same position (in meters).
        self.tolerance = rospy.get_param('~tolerance', 0.1)

        # Reference frame of the workspace.
        self.reference_frame = rospy.get_param('~reference_frame', 'object')

        # Scale to visualize the points in the workspace.
        self.scale = rospy.get_param('~scale', 0.01)

        # Node cycle rate (in Hz).
        self.loop_rate = rospy.Rate(rospy.get_param('~loop_rate', 100))

        # Publishers
        self.event_out = rospy.Publisher("~event_out", std_msgs.msg.String, queue_size=10)
        self.workspace_points_pub = rospy.Publisher(
            "~workspace_points", visualization_msgs.msg.Marker, queue_size=10)
        self.covered_points_pub = rospy.Publisher(
            "~covered_points", visualization_msgs.msg.Marker, queue_size=10)

        # Subscribers
        rospy.Subscriber("~event_in", std_msgs.msg.String, self.event_in_cb)
        rospy.Subscriber(
            '~current_point', geometry_msgs.msg.PointStamped, self.current_point_cb)

        self.workspace_points = visualization_msgs.msg.Marker()
        self.workspace_points.header.frame_id = self.reference_frame
        self.workspace_points.type = visualization_msgs.msg.Marker.POINTS
        self.workspace_points.scale = geometry_msgs.msg.Vector3(self.scale, self.scale, self.scale)
        self.workspace_points.color = std_msgs.msg.ColorRGBA(0.75, 0.75, 0.75, 0.25)
        self.workspace_points.points = to_point_list(self.workspace)

        self.covered_points = visualization_msgs.msg.Marker()
        self.covered_points.header.frame_id = self.reference_frame
        self.covered_points.type = visualization_msgs.msg.Marker.POINTS
        self.covered_points.scale = geometry_msgs.msg.Vector3(self.scale, self.scale, self.scale)
        self.covered_points.color = std_msgs.msg.ColorRGBA(1.0, 0.0, 0.0, 0.75)

    def event_in_cb(self, msg):
        """
        Obtains the event for the node (e.g. start, stop).

        :param msg: Event message for the node.
        :type msg: std_msgs.msg.String

        """
        self.event = msg.data

    def current_point_cb(self, msg):
        """
        Obtains the current point.

        :param msg: Current point.
        :type msg: geometry_msgs.msg.PointStamped

        """
        self.current_point = msg

    def start(self):
        """
        Starts the node.

        """
        rospy.loginfo("Ready to start...")
        state = 'INIT'

        while not rospy.is_shutdown():

            if state == 'INIT':
                state = self.init_state()
            elif state == 'IDLE':
                state = self.idle_state()
            elif state == 'RUNNING':
                state = self.running_state()

            rospy.logdebug("State: {0}".format(state))
            self.loop_rate.sleep()

    def init_state(self):
        """
        Executes the INIT state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_start':
            rospy.loginfo("Loading covered workspace...")
            self.load_covered_workspace()
            return 'IDLE'
        else:
            return 'INIT'

    def idle_state(self):
        """
        Executes the IDLE state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            rospy.loginfo("Saving covered workspace...")
            self.save_covered_workspace()
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        elif self.current_point is not None:
            return 'RUNNING'
        else:
            return 'IDLE'

    def running_state(self):
        """
        Executes the RUNNING state of the state machine.

        :return: The updated state.
        :rtype: str

        """
        if self.event == 'e_stop':
            rospy.loginfo("Saving covered workspace...")
            self.save_covered_workspace()
            self.event_out.publish('e_stopped')
            self.reset_component_data()
            return 'INIT'
        else:
            self.workspace_points_pub.publish(self.workspace_points)

            point_stamped = geometry_msgs.msg.PointStamped()
            point = self.listener.transform_point(
                self.current_point.point, self.current_point.header, self.reference_frame)
            point_stamped.point = point

            index = self.point_in_workspace(point_stamped, self.tolerance)
            if index not in self.covered_points_idx:
                self.covered_points_idx.add(index)
                self.covered_points.points.append(to_point(self.workspace[index]))
                self.covered_points_pub.publish(self.covered_points)
                if len(self.covered_points_idx) % 10 == 0:
                    rospy.loginfo("Percentage covered: {:.2f}%".format(
                        100 * len(self.covered_points_idx) / float(len(self.workspace))))

            self.reset_component_data()
            return 'IDLE'

    def point_in_workspace(self, point, tolerance):
        """
        Checks if a point is within tolerance of a point in the discretized workspace.

        :param point: Point.
        :type point: geometry_msgs.msg.PointStamped

        :param tolerance: Tolerance to check if a point is within another point of the workspace.
        :type tolerance: float

        :return: The index of the point in the workspace, if the point is not in the workspace it returns None.
        :rtype: int

        """
        p = np.array([point.point.x, point.point.y, point.point.z])
        distances = np.linalg.norm(self.workspace - p, axis=1)
        if min(distances) <= tolerance:
            return np.argmin(distances)
        else:
            return None

    def save_covered_workspace(self):
        """
        Saves the current points covering the workspace.

        """
        np.save(self.config_path + 'covered_workspace.npy', self.covered_points.points)
        np.save(self.config_path + 'covered_workspace_idx.npy', self.covered_points_idx)

    def load_covered_workspace(self):
        """
        Loads the points previously that previously covered the workspace.

        """
        self.covered_points.points = np.load(self.config_path + 'covered_workspace.npy').tolist()
        self.covered_points_idx = np.load(self.config_path + 'covered_workspace_idx.npy').tolist()

    def reset_component_data(self):
        """
        Clears the data of the component.

        """
        self.current_point = None
        self.event = None


def to_point(arr):
    """
    Converts a numpy array describing a 3D point into a geometry_msgs/Point message.

    :param arr: Array to convert.
    :type arr: numpy array.

    :return: The converted array.
    :rtype: geometry_msgs/Point

    """
    return geometry_msgs.msg.Point(x=arr[0], y=arr[1], z=arr[2])


def to_point_list(arr):
    """
    Converts a numpy array having a list of 3D points into an array of geometry_msgs/Point messages.

    :param arr: Array to convert (of shape nx3).
    :type arr: numpy array.

    :return: The converted array.
    :rtype: geometry_msgs/Point[]

    """
    x = np.copy(arr)
    points = []

    for point in x:
        points.append(geometry_msgs.msg.Point(x=point[0], y=point[1], z=point[2]))

    return points


def main():
    rospy.init_node("track_workspace_coverage_node", anonymous=True)
    track_workspace_coverage_node = TrackWorkspaceCoverageNode()
    track_workspace_coverage_node.start()
